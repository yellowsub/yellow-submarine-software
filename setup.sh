#!/bin/bash

# Delete old .bashrc code
perl -i -p0e 's/\n\n# yellow-submarine-software.*# end yellow-submarine-software//s' ~/.bashrc

# Add newline and comment to .bashrc
echo '' >> ~/.bashrc
echo '# yellow-submarine-software components' >> ~/.bashrc

# move to the folder that contains this file
if echo "$0" | grep 'setup.sh'; then
	cd "${0/setup.sh/}"
else
	# in case this was called with sudo
	cd "${1/setup.sh/}"
fi

# move to submodules folder
cd modules

# librealsense setup
#cd librealsense
#git pull origin master
#mkdir build
#cd build
#cmake ..
#sudo make uninstall
#make clean
#make -j4
#sudo make install
#cd ../..

# freebuoyancy_gazebo setup
cd freebuoyancy_gazebo
git pull origin master
mkdir build
cd build
cmake ..
make -j4
sudo make install
cd ../..

# ardupilot_gazebo setup
cd ardupilot_gazebo
git fetch --tags
git checkout add_link
git pull origin add_link
mkdir build
cd build
cmake ..
make -j4
sudo make install
cd ..
echo 'source /usr/share/gazebo/setup.sh' >> ~/.bashrc
cd ..

# bluerov_ros_playground setup
cd bluerov_ros_playground
git pull origin master
echo 'export GAZEBO_MODEL_PATH='$PWD'/model:${GAZEBO_MODEL_PATH}' >> ~/.bashrc
echo 'export GAZEBO_RESOURCE_PATH='$PWD'/worlds:${GAZEBO_RESOURCE_PATH}' >> ~/.bashrc
cd ..

# ardupilot setup
cd ardupilot
git fetch --tags
git checkout Sub-4.0
git pull origin Sub-4.0
git submodule update --init --recursive
make clean
./waf configure
./waf build sub
cd ..

# Run bashrc with updated components
echo '# end yellow-submarine-software' >> ~/.bashrc
source ~/.bashrc
