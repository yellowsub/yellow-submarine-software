# Software

## System Requirements

- Ubuntu 18.04 LTS
- Graphics card with 2GB of memory

## To install this code

1. Install prerequisites:
    1. ``sudo apt install git python-pip ros-desktop-full libgazebo9-dev``
    2. ``pip install --upgrade pip``
        - This step is optional, but recommended. If you do this step, restart
          your computer before continuing.
    3. ``pip install --user pyrealsense2 pymavlink MAVProxy``
    4. ``sudo apt install python3-pip``
        - pymavlink will not work if you install python3-pip before installing
          pymavlink.
2. Download and install repository
    1. ``git clone --recursive
       https://gitlab.com/yellowsub/yellow-submarine-software.git``
    2. ``cd yellow-submarine-software``
    3. ``./setup.sh``

## To use this code

### Simulation

The following links will be helpful:

- https://www.ardusub.com/developers/sitl.html
- https://www.ardusub.com/operators-manual/flight-modes.html
- https://www.ardusub.com/operators-manual/rc-input-and-output.html

The standard process for running the gazebo simulation:
1. Open a terminal in the ``bluerov_ros_playground`` directory
2. Run ``gazebo worlds/underwater.world -u``
3. Press the Play button (bottom left corner of gazebo)
4. Open a new terminal in the ``ardupilot/Tools/autotest`` directory
5. Run ``./sim_vehicle.py -f gazebo-bluerov2 -L RATBeach --out=udp:0.0.0.0:14550
   --console -v ArduSub``
6. ``arm throttle``
7. Use rc controls to move the ROV around
    - To move the craft forward/backward, use the command ``rc 5 <speed>``
    - Other channels (yaw, roll, etc.) can be found 
      [here](https://www.ardusub.com/operators-manual/rc-input-and-output.html)
    - Speed settings vary from 0 to 3000, where 0 is full-backward, 1500 is no
      movement, and 3000 is full-forward.

## For Developers

If you don't document it, nobody else can use it. Comment your code, modify
``setup.sh`` as necessary, and create/modify READMEs like this one to ensure
that every other dev can follow in your footsteps.

Please keep your works-in-progress in branches to keep the ``master`` branch
stable. This is most easily done with the command
``git checkout -b "branch_name"``.

### Helpful git components

#### Configure your username and email for future commits

``git config --global user.name "Your Name"``

``git config --global user.email "your.email@domain.com"``

#### Store your GitLab username and password locally

``git config --global credential.helper store``

#### Notepad++

If you are unfamiliar with vim, Notepad++ is a much more user-friendly
alternative. After downloading and installing it, you can set it as git's
default text editor with the command

``git config --global core.editor notepad-plus-plus``

#### Submodules

If you need to use code from another repository for your work, please include it
as a git submodule. The current submodules of this project can be found in the
``modules`` folder. To add the YOLACT submodule to the repository, we used the
command

``git submodule add https://github.com/dbolya/yolact modules/yolact/``

Please ensure your submodules end up in the ``modules`` folder like the others,
and put any required setup commands into ``setup.sh``.
